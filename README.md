## Setup

1. copy the .env.example to a new .env file and tailor the file to your prefferences
2. run `composer install` to install all dependencies
3. run `php artisan migrate` to create the database
4. run `php artisan db:seed` to initially seed the database
5. run `php artisan serve` to start the build-in webserver
6. Enjoy