<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert([
            'name' => 'Motivo',
            'email' => 'welkom@motivo.nl',
            'email_verified_at' => now(),
            'password' => bcrypt('Test123!'),
        ]);
        DB::table('users')->insert([
            'name' => 'Christiaan',
            'email' => 'christiaan@motivo.nl',
            'email_verified_at' => now(),
            'password' => bcrypt('chris'),
        ]);

        factory(App\User::class, 15)->create();    
    }
}
