<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['register' => false, 'reset' => false]);

Route::get('/home', 'HomeController@index')->name('home');

Route::prefix('group')->group(function () {
    Route::get('list', 'GroupController@index')->name('group_list');
    Route::get('create', 'GroupController@create')->name('group_create');
    Route::put('store', 'GroupController@store')->name('group_store');
    Route::get('edit/{id}', 'GroupController@edit')->name('group_edit');
    Route::patch('update/{id}', 'GroupController@update')->name('group_update');
    Route::delete('delete/{id}', 'GroupController@delete')->name('group_delete');

});