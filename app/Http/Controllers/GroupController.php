<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Group;
use App\User;

class GroupController extends Controller
{
        public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the groups.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groups = Group::all();

        return view('groups.index', compact('groups'));
    }

    /**
     * Show the form for creating a new group.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('groups.create');
    }

    /**
     * Store a newly created group in the database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $request->validate([
        'name'=>'required'
      ]);
      $group = new Group([
        'name' => $request->get('name'),
        'description'=> $request->get('description'),
        'created_by'=> $request->user()->id,
      ]);
      $group->save();
      $group->users()->attach([$request->user()->id]);

      return redirect('/group/list')->with('success', 'Group has been added');
    }

    /**
     * Show the form for editing the specified group.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(int $id)
    {
        $group = Group::with('users:id,name')->find($id);
        $groupUserIds = array_column($group->users->toArray(),'id');
        $users = User::all();

        return view('groups.edit', compact('group','users', 'groupUserIds'));
    }

    /**
     * Update the specified group in the database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {
        $request->validate([
            'name'=>'required'
        ]);
        $group = Group::find($id);
        $group->name = $request->get('name');
        $group->description = $request->get('description');
        $group->save();
        $group->users()->sync($request->input('users'));

        return redirect('/group/list')->with('success', 'Group has been updated');
    }

    /**
     * Remove the specified group from the database.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(int $id)
    {
        $group = Group::find($id);
        $group->users()->sync([]);
        $group->delete();

      return redirect('/group/list')->with('success', 'Group has been deleted successfully. You may now hide under your desk');
    }
}
