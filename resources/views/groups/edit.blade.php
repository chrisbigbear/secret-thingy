@extends('layouts.app')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="card uper">
  <div class="card-header">
    Edit Group
  </div>
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('group_update', $group->id) }}">
        @method('PATCH')
        @csrf
        <div class="form-group">
          <label for="name">Group Name:</label>
          <input type="text" class="form-control" name="name" value='{{ $group->name }}' />
        </div>
        <div class="form-group">
          <label for="price">Group Description:</label>
          <input type="text" class="form-control" name="description" value='{{ $group->description }}' />
        </div>
        <div class="form-group">
          <label for="users">Group Users:</label>
          <select class="form-control" name="users[]" multiple="multiple">
            @foreach ($users as $user)
                  <option value='{{$user->id}}'
                  @if(in_array($user->id,$groupUserIds))
                      selected="selected"
                  @endif
                >{{ $user->name }}</option>
            @endforeach
          </select>
        </div>
        <button type="submit" class="btn btn-primary">Update</button>
      </form>
  </div>
</div>
@endsection