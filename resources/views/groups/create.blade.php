@extends('layouts.app')

@section('content')
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="card uper">
  <div class="card-header">
    Create Group
  </div>
  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('group_store') }}">
        @method('PUT')
        @csrf
        <div class="form-group">
          <label for="name">Group Name:</label>
          <input type="text" class="form-control" name="name"/>
        </div>
        <div class="form-group">
          <label for="price">Group Description:</label>
          <input type="textarea" class="form-control" name="description"/>
        </div>
        <button type="submit" class="btn btn-primary">Create</button>
      </form>
  </div>
</div>
@endsection